<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/eticaret/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/eticaret/');

// DIR
define('DIR_APPLICATION', 'C:/wamp64/www/eticaret/catalog/');
define('DIR_SYSTEM', 'C:/wamp64/www/eticaret/system/');
define('DIR_IMAGE', 'C:/wamp64/www/eticaret/image/');
define('DIR_LANGUAGE', 'C:/wamp64/www/eticaret/catalog/language/');
define('DIR_TEMPLATE', 'C:/wamp64/www/eticaret/catalog/view/theme/');
define('DIR_CONFIG', 'C:/wamp64/www/eticaret/system/config/');
define('DIR_CACHE', 'C:/wamp64/www/eticaret/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/wamp64/www/eticaret/system/storage/download/');
define('DIR_LOGS', 'C:/wamp64/www/eticaret/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/wamp64/www/eticaret/system/storage/modification/');
define('DIR_UPLOAD', 'C:/wamp64/www/eticaret/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'umut');
define('DB_PASSWORD', '12345');
define('DB_DATABASE', 'eticaret');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
